from io import StringIO

import pytest
from netlink.sap.rfc import Landscape


LANDSCAPE_XML = """<Landscape>
    <Workspaces>
        <Workspace uuid = "11111111-1111-1111-1111-111111111111" name = "Local">
            <Item uuid = "11111111-1111-1111-1111-111111111112" serviceid = "22222222-2222-2222-2222-222222222221"/>
            <Item uuid = "11111111-1111-1111-1111-111111111113" serviceid = "22222222-2222-2222-2222-222222222222"/>
        </Workspace>
    </Workspaces>
    <Services>
        <Service
            type = "SAPGUI"
            uuid = "22222222-2222-2222-2222-222222222221"
            name = "ABC"
            systemid = "ABC"
            mode = "1"
            server = "abc.sap.nowhere:3200"
            sncop = "-1"
            sapcpg = "1100"
            clocale = "EN"
            dcpg = "4110"/>
        <Service
            type = "SAPGUI"
            uuid = "22222222-2222-2222-2222-222222222222"
            name = "DEF"
            systemid = "DEF"
            mode = "1"
            server = "def.sap.nowhere:3201"
            sncop = "-1"
            sapcpg = "1100"
            clocale = "EN"
            dcpg = "4110"/>
    </Services>
</Landscape>
"""


@pytest.fixture
def landscape():
    landscape_file = StringIO(LANDSCAPE_XML)
    return Landscape(landscape_file)


def test_abc(landscape):
    assert landscape['abc']['ashost'] == 'abc.sap.nowhere'
    assert landscape['abc']['sysnr'] == '00'
